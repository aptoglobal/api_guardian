require 'httparty'

module ApiGuardian
  module Strategies
    module Registration
      class Kakao < Base
        provides_registration_for :kakao

        allowed_api_parameters :access_token, :email

        def register(store, attributes)
          super(attributes)

          response = kakao_client(attributes).authorize!

          data = build_user_attributes_from_response(response, attributes)
          identity_data = build_identity_attributes_from_response(response, attributes[:access_token])

          # create user
          instance = store.new(nil)
          user = instance.create_with_identity(data, identity_data, confirm_email: true)

          # TODO: put user created event onto queue

          user
        end

        def register_provider_with_user(user, attributes)
          super(user, attributes)

          response = kakao_client(attributes).authorize!
          identity_data = build_identity_attributes_from_response(response, attributes[:access_token])
          user.identities.create(identity_data)
        end

        def build_user_attributes_from_response(response, attributes = {})
          full_name = response['properties']['nickname']

          password, password_confirmation = prep_passwords attributes

          {
            full_name: full_name,
            email: response['kakao_account']['is_email_verified'] ? response['kakao_account']['email'] : nil,
            email_confirmed_at: DateTime.now.utc,
            role_id: ApiGuardian::Stores::RoleStore.default_role.id,
            active: true,
            password: password,
            password_confirmation: password_confirmation
          }
        end

        def build_identity_attributes_from_response(response, access_token)
          {
            provider: 'kakao',
            provider_uid: response['id'],
            tokens: { access_token: access_token }
          }
        end

        protected

        def kakao_client(attributes)
          ApiGuardian::Helpers::Kakao.new(attributes[:access_token])
        end
      end
    end
  end
end
