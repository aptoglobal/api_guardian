module ApiGuardian
  module Strategies
    module Authentication
      class Apple < Base
        provides_authentication_for :apple

        def authenticate(access_token)
          # Get Apple JWT
          client = ApiGuardian::Helpers::Apple.new(access_token)
          response = client.authorize!

          # Find identity exists for apple for this user, or fallback to a user with the same registered email
          identity = ApiGuardian::Identity.find_by(provider: :apple, provider_uid: response['sub'])
          user = ApiGuardian.configuration.user_class.find_by(id: identity&.user_id) || ApiGuardian::Stores::UserStore.new.find_by_email(response['email'])

          # Verify user is active (which is all that super does)
          super(user: user)
          
          if user
            identity ||= user.identities.create(provider: :apple, provider_uid: response['sub'], tokens: { access_token: access_token })
            update_identity(identity, access_token)
          end

          user
        end

        def update_identity(identity, access_token)
          identity.update_attributes(
            tokens: { access_token: access_token }
          )
        end
      end
    end
  end
end
