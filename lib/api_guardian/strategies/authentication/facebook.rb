module ApiGuardian
  module Strategies
    module Authentication
      class Facebook < Base
        provides_authentication_for :facebook

        def authenticate(access_token)
          # Get FB user object
          client = ApiGuardian::Helpers::Facebook.new(access_token)
          response = client.authorize!

          # Find identity exists for facebook for this user, or fallback to a user with the same registered email
          identity = ApiGuardian::Identity.find_by(provider: :facebook, provider_uid: response['id'])
          user = ApiGuardian.configuration.user_class.find_by(id: identity&.user_id) || ApiGuardian::Stores::UserStore.new.find_by_email(response['email'])

          # Verify user is active (which is all that super does)
          super(user: user)

          if user
            identity ||= user.identities.create(provider: :facebook, provider_uid: response['id'], tokens: { access_token: access_token })
            update_identity(identity, access_token)
          end

          user
        end

        def update_identity(identity, access_token)
          identity.update_attributes(
            tokens: { access_token: access_token }
          )
        end
      end
    end
  end
end
