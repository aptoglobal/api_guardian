module ApiGuardian
  module Strategies
    module Authentication
      class Kakao < Base
        provides_authentication_for :kakao

        def authenticate(access_token)
          # Get Kakao user object
          client = ApiGuardian::Helpers::Kakao.new(access_token)
          response = client.authorize!

          # Find identity exists for Kakao for this user, or fallback to a user with the same registered email
          identity = ApiGuardian::Identity.find_by(provider: :kakao, provider_uid: response['id'])
          user = 
            if identity
              ApiGuardian.configuration.user_class.find_by(id: identity.user_id)
            elsif response['kakao_account'] && response['kakao_account']['is_email_verified']
              ApiGuardian::Stores::UserStore.new.find_by_email(response['kakao_account']['email'])
            else
              nil
            end

          # Verify user is active (which is all that super does)
          super(user: user)

          if user
            identity = user.identities.create(provider: :kakao, provider_uid: response['id'], tokens: { access_token: access_token })
            update_identity(identity, access_token)
          end

          user
        end

        def update_identity(identity, access_token)
          identity.update_attributes(
            tokens: { access_token: access_token }
          )
        end
      end
    end
  end
end
