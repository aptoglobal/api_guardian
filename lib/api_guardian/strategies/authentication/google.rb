module ApiGuardian
  module Strategies
    module Authentication
      class Google < Base
        provides_authentication_for :google

        def authenticate(access_token)
          # Get Google user object
          client = ApiGuardian::Helpers::Google.new(access_token)
          response = client.authorize!

          # Find identity exists for google for this user, or fallback to a user with the same registered email
          identity = ApiGuardian::Identity.find_by(provider: :google, provider_uid: response['sub'])
          user = ApiGuardian.configuration.user_class.find_by(id: identity&.user_id) || ApiGuardian::Stores::UserStore.new.find_by_email(response['email'])

          # Verify user is active (which is all that super does)
          super(user: user)

          if user
            identity ||= user.identities.create(provider: :google, provider_uid: response['sub'], tokens: { access_token: access_token })
            update_identity(identity, access_token)
          end

          user
        end

        def update_identity(identity, access_token)
          identity.update_attributes(
            tokens: { access_token: access_token }
          )
        end
      end
    end
  end
end
