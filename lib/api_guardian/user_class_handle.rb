module ApiGuardian
  class UserClassHandle
    def to_s
      fail "'ApiGuardian.configuration.user_class' has not been set yet." unless ApiGuardian.configuration.user_class
      "::#{ApiGuardian.configuration.user_class.to_s}"
    end
  end
end