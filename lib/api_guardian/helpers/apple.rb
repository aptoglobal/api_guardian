require 'apple_auth'

module ApiGuardian
  module Helpers
    class Apple
      attr_reader :access_token

      def initialize(access_token)
        @access_token = access_token
      end

      def authorize!
        user_id, jwt = @access_token.split(':')
        user_id = JWT.decode(jwt, nil, false).first['sub'] if user_id.blank?
        payload = AppleAuth::UserIdentity.new(user_id, jwt).validate!

        payload.with_indifferent_access
      end

      def revoke!
        _, jwt = @access_token.split(':')
        response = HTTParty.post('https://appleid.apple.com/auth/revoke', 
          headers: {
            'Content-Type' => 'application/x-www-form-urlencoded'
          },
          body: {
            client_id: AppleAuth.config.apple_client_id,
            client_secret: generate_client_secret,
            token: jwt,
            token_type_hint: 'access_token'
          }
        )
        response.code == 200
      end

    protected

      def generate_client_secret
        JWT.encode(
          {
            iss: AppleAuth.config.apple_team_id,
            iat: Time.now.to_i,
            exp: (Time.now + 5.minutes).to_i,
            aud: 'https://appleid.apple.com',
            sub: AppleAuth.config.apple_client_id
          },
          AppleAuth.config.apple_private_key,
          'ES256',
          {
            kid: AppleAuth.config.apple_key_id
          }
        )
      end
    end
  end
end
