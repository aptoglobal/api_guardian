require 'httparty'

module ApiGuardian
  module Helpers
    class Kakao
      attr_reader :access_token

      def initialize(access_token)
        @access_token = access_token
      end

      def authorize!
        kakao_response = HTTParty.get('https://kapi.kakao.com/v2/user/me', headers: { 'Authorization' => "Bearer #{@access_token}"})

        Rails.logger.debug("Retrieved response from Kakao: #{kakao_response.to_json}")

        fail(ApiGuardian::Errors::IdentityAuthorizationFailed, "Could not connect to Kakao: #{kakao_response.to_json}") unless kakao_response.code == 200

        kakao_response.parsed_response
      end
    end
  end
end
