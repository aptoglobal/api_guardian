require 'google-id-token'

module ApiGuardian
  module Helpers
    class Google
      attr_reader :access_token

      def initialize(access_token)
        @access_token = access_token
      end

      def authorize!
        validator = GoogleIDToken::Validator.new
        auth_client = JWT.decode(@access_token, nil, false).first['aud'] rescue nil
        unless ApiGuardian.configuration.google_auth_clients.include?(auth_client)
          fail ApiGuardian::Errors::IdentityAuthorizationFailed, "Google access token is not issued for valid audience (#{auth_client.inspect})"
        end

        payload = 
          begin
            validator.check(@access_token, auth_client)
          rescue GoogleIDToken::AudienceMismatchError
            nil
          rescue GoogleIDToken::ValidationError => e
            ApiGuardian.logger.error "Google authorization failed! #{e.message}: #{@access_token.inspect}"
            nil
          end
        fail ApiGuardian::Errors::IdentityAuthorizationFailed, "Could not connect to Google" if payload.nil?

        payload
      end
    end
  end
end
