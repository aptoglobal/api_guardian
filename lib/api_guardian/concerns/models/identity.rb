require 'active_support/concern'

module ApiGuardian
  module Concerns
    module Models
      module Identity
        extend ActiveSupport::Concern

        included do
          self.table_name = 'api_guardian_identities'

          belongs_to :user, class_name: ApiGuardian::UserClassHandle.new

          validates :provider, presence: true
          validates :provider_uid, presence: true, uniqueness: {
            scope: :provider, message: 'UID already exists for this provider.'
          }

          after_destroy :revoke_tokens

          def revoke_tokens
            if provider == 'apple'
              ApiGuardian::Helpers::Apple.new(tokens.with_indifferent_access[:access_token]).revoke!
            end
          end
        end
      end
    end
  end
end
